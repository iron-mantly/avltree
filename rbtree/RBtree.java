package rbtree;

import java.util.concurrent.BlockingQueue;

import static rbtree.RBTreeNode.BLACK;
import static rbtree.RBTreeNode.RED;
/*
/ 红黑树
 */
public class RBtree {

    public RBTreeNode root = null;
    public int size = 0;

    public boolean insert(long key){
        if (root==null){
            root = new RBTreeNode(key,null,BLACK);
            size++;
            return true;
        }
        //查找到插入的位置
        RBTreeNode curr = root;
        RBTreeNode parent = null;
        while(curr!=null){

            if (key == curr.key){
                return false;
            }else if (key<curr.key){
                parent = curr;
                curr = curr.left;

            }else{
                parent = curr;
                curr = curr.right;
            }
        }
        /**
         * 根据插入规则   再次插入的节点一定是红色的
         * */
        RBTreeNode node = new RBTreeNode(key,parent,RED);
        if (key<parent.key){
            parent.left = node;
        }else{
            parent.right = node;
        }
        //开始调整 进行红黑树规则的判断 + 平衡调整过程
        adjustBalance(node);
        size++;
        return true;
    }

    private void adjustBalance(RBTreeNode node) {

        while(true){
            RBTreeNode parent = node.parent;
            if (parent==null){
                break;
            }
            if (parent.color==BLACK){
                break;
            }


            //一定破坏了 红色不能相邻的规则
            RBTreeNode grandpa = parent.parent;
            if (parent == grandpa.left){
                //找叔叔 节点
                RBTreeNode uncle = grandpa.right;
                if (uncle!=null&&uncle.color==RED){
                    /**
                     * 情况1：叔叔存在 并且 叔叔的颜色是红色
                     * 步骤：
                     * 1. 叔叔和父亲的颜色改成黑色
                     * 2. 祖父的颜色改成红色
                     * 3. 把祖父视为 node，再去判断是否违反规则了
                     */
                    grandpa.color = RED;
                    parent.color = uncle.color =BLACK;
                    node = grandpa;
                    continue;
                }else{
                    //关系不一致 先左旋
                    if (node == parent.right){

                        leftOrRotate(parent);
                        //交换节点
                        //swap(parent,node);
                        parent = node;
                    }
                    //关系一致 右旋
                    rightOrRotate(grandpa);
                    grandpa.color =RED;
                    parent.color = BLACK;
                    break;
                }

            }else{
                //找叔叔 节点
                RBTreeNode uncle = grandpa.left;
                if (uncle!=null&&uncle.color==RED){
                    /**
                     * 情况1：叔叔存在 并且 叔叔的颜色是红色
                     * 步骤：
                     * 1. 叔叔和父亲的颜色改成黑色
                     * 2. 祖父的颜色改成红色
                     * 3. 把祖父视为 node，再去判断是否违反规则了
                     */
                    grandpa.color = RED;
                    parent.color = uncle.color =BLACK;
                    node = grandpa;
                    continue;
                }else{
                    //关系不一致 先右旋
                    if (node==parent.left){
                        rightOrRotate(parent);

                        //swap(parent,node);
                        parent = node;
                    }
                    //关系一致 左旋
                    leftOrRotate(grandpa);
                    grandpa.color= RED;
                    parent.color = BLACK;
                    break;
                }

            }
        }
        /**
         * 无论之前是什么情况，统一把根改成黑色
         * 走到此处时，root 一定不是 null
         */
        root.color = BLACK;
    }

    private void swap(RBTreeNode parent, RBTreeNode node) {
        RBTreeNode node1 = node;
        node = parent;
        parent = node1;
    }


    //左旋
    private void leftOrRotate(RBTreeNode node) {
        RBTreeNode parent = node.parent;
        RBTreeNode right = node.right;
        RBTreeNode rightOrLeft = right.left;

        right.parent = parent;
        if (parent==null){
            root = right;
        }else{
            if (node==parent.left){
                parent.left = right;
            }else{
                parent.right=right;
            }
        }

        right.left = node;
        node.parent = right;

        node.right  = rightOrLeft;
        if (rightOrLeft!=null){
            rightOrLeft.parent = node;
        }
    }

    private void rightOrRotate(RBTreeNode node) {

        RBTreeNode parent = node.parent;
        RBTreeNode left = node.left;
        RBTreeNode leftOrRight = left.right;

        left.parent = parent;
        if (parent==null){
            root  =left;
        }else{
            if (node==parent.left){
                parent.left = left;
            }else{
                parent.right=left;
            }
        }

        left.right = node;
        node.parent = left;

        node.left = leftOrRight;
        if (leftOrRight!=null){
            leftOrRight.parent = left;
        }
    }
}
