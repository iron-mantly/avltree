package rbtree;

public class RBTreeNode {

    /**
     * 为了简单起见：
     * 1. 不写 key-value 形式，只写纯 key 形式
     * 2. key 的类型固定成 long 类型
     * 3. 全部用 public 修饰符
     *
     * 红黑树中树的结点
     */
    public static final boolean BLACK = true;
    public static final boolean RED = false;

    public long key;

    public RBTreeNode left;
    public RBTreeNode right;

    public RBTreeNode parent;
    public boolean color;

    public RBTreeNode(long key ,RBTreeNode parent, boolean color){
        this.key = key;
        this.left=this.right = null;
        this.parent = parent;
        this.color = color;
    }

    @Override
    public String toString() {
        return "RBTreeNode{" +
                "key=" + key +
                ", color=" + (color==BLACK ? "黑":"红") +
                '}';
    }
}
