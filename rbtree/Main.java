package rbtree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Long> list = new ArrayList<>();
        Random random = new Random(20210821);

        for (long i = 1; i <= 20; i++) {
            list.add(i);
        }

        Collections.shuffle(list, random);

        RBtree tree = new RBtree();
        for (long key : list) {
            tree.insert(key);
            System.out.println(key);
        }

        System.out.println("插入结束");
    }
}
