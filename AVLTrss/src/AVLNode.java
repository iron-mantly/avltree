// 不包含 value
// key 的类型也是 long
// 结点中需要多保存两个字段
// 1. 以该结点为根的子树的平衡因子 bf
// 2. 该结点的父结点 parent
public class AVLNode {
    public long key;

    public AVLNode left;
    public AVLNode right;

    // AVL 树中的特殊字段
    public int bf;
    public AVLNode parent;

    @Override
    public String toString() {
        return "AVLNode{" +
                "key=" + key +
                ", bf=" + bf +
                '}';
    }
}
