import java.util.*;

public class Tst {
    public static void main(String[] args) {
        //test1();
        test2();
    }

    private static void test2() {
        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            AVLTree tree = new AVLTree();
            for (int j = 0; j < 1_0000; j++) {
                int key = random.nextInt(10_0000);
                tree.insert(key);
            }

            validate(tree);
        }
    }

    private static void validate(AVLTree tree) {
        validateIsSearchTree(tree);
        validateIsBalanceTree(tree);
        System.out.println("这棵树是 AVL 树");
    }

    private static int heightAndCheckBF(AVLNode root) {
        if (root == null) {
            return 0;
        }

        int leftHeight = heightAndCheckBF(root.left);
        int rightHeight = heightAndCheckBF(root.right);

        if (root.bf != (leftHeight - rightHeight)) {
            throw new RuntimeException("有结点 bf 计算不正确");
        }

        if (root.bf != -1 && root.bf != 0 && root.bf != 1) {
            throw new RuntimeException("有结点，左右子树高度差的绝对值超过了 1，不是平衡树");
        }

        return Integer.max(leftHeight, rightHeight) + 1;
    }

    private static void validateIsBalanceTree(AVLTree tree) {
        heightAndCheckBF(tree.root);
        System.out.println("所有结点的 bf 都计算正确，并且都在范围内，是平衡树");
    }

    private static void validateIsSearchTree(AVLTree tree) {
        List<Long> inorderKeys = new ArrayList<>();
        inorderSaveKey(inorderKeys, tree.root);
        // inorderKeys 中保存的中序遍历后所有 key

        // 复制出得到的所有 key，对得到的 key 进行排序
        List<Long> keysSorted = new ArrayList<>(inorderKeys);
        Collections.sort(keysSorted);

        if (keysSorted.equals(inorderKeys)) {
            System.out.println("中序遍历是有序的，说明是搜索树");
        } else {
            throw new RuntimeException("中序遍历是无序的，说明不是搜索树");
        }
    }

    private static void inorderSaveKey(List<Long> inorderKeys, AVLNode root) {
        if (root != null) {
            inorderSaveKey(inorderKeys, root.left);
            inorderKeys.add(root.key);
            inorderSaveKey(inorderKeys, root.right);
        }
    }


    private static void test1() {
        List<Integer> list = Arrays.asList(2, 15, 8, 3, 4, 6, 9, 7, 17, 20, 19, 14);

        AVLTree tree = new AVLTree();
        for (Integer key : list) {
            tree.insert(key);
        }

        preorder(tree.root);
        System.out.println();
        inorder(tree.root);
        System.out.println();
    }

    private static void preorder(AVLNode node) {
        if (node != null) {
            System.out.printf("(%d, %d) ", node.key, node.bf);
            preorder(node.left);
            preorder(node.right);
        }
    }

    private static void inorder(AVLNode node) {
        if (node != null) {
            inorder(node.left);
            System.out.printf("(%d, %d) ", node.key, node.bf);
            inorder(node.right);
        }
    }
}
